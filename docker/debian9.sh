#!/bin/sh
#
test "$1" = '-h' && exec echo "Usage: $0 [-i]"
TOP=$(readlink -f $(dirname $0)/..)
test -f /.dockerenv || exec docker run -ti -v $TOP:/test debian:9 bash /test/docker/$(basename $0) $1
cd /test
interp="sh -x"
intera=echo
test "$1" = '-i' && interp="cat" && intera=bash

export PROJECT=isv:ownCloud:desktop:testing
export APPLICATION_EXECUTABLE="owncloud"
export LINUX_PACKAGE_SHORTNAME="owncloud"
export REPOSITORY_ENDPOINT="https://software.opensuse.org/download.json?project=$PROJECT&package=$LINUX_PACKAGE_SHORTNAME-client"
export REPO="http://download.opensuse.org/repositories/$PROJECT"
export DEBIAN_FRONTEND=noninteractive
apt-get update && apt-get -y install ca-certificates wget python3 apt-transport-https gnupg2 
cat << EOF | $interp
# ------------------------------------------------------
# docker environment up. Now run the following commands:

./setup_installation.py -d Debian -v 9.0 -o '$REPO/Debian_9.0/'
apt-get -y --allow-unauthenticated --force-yes install $LINUX_PACKAGE_SHORTNAME-client
dpkg -l $APPLICATION_EXECUTABLE-client
$APPLICATION_EXECUTABLE'cmd' --version

EOF
$intera
