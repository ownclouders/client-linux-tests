#!/usr/bin/env python3

'''
 Script to set up the package manager to install owncloud-client.
 Unfixed issue: https://github.com/openSUSE/software-o-o/issues/123#issuecomment-333521226
 please use --override to circumvent
'''
import os
import subprocess
import urllib.request, re, base64
from urllib.parse import urlparse, parse_qs, urlsplit
import json
from optparse import OptionParser

def get_repo_url(url,distribution,version):
        """
        Supported url formats:

        http://obs.example.com/desktop/client-2.3.3
                with apache listing and optional subdirectory named distribution_version
        and
        https://software.opensuse.org//download.json?project=isv:ownCloud:desktop&package=owncloud-client
                with *.repo files named after the project url parameter.

        Response: url of the *.repo file (if any), or full url of the distribution_version folder.
        """

        if url is None: raise ValueError("url is None")
        if distribution is None: raise ValueError("distribution is None")
        if version is None: raise ValueError("version is None")

        tag = distribution+"_"+version
        o = urlsplit(url)
        if o.username:
                auth = o.username
                if o.password: auth += ':'+o.password
                authb64 = base64.b64encode(bytes(auth,encoding='utf-8'))

                plainurl = o.scheme+'://'+o.hostname
                if o.port: plainurl += ':'+str(o.port)
                plainurl += o.path
                if o.query: plainurl += '?'+o.query

                request = urllib.request.Request(plainurl)
                request.add_header("Authorization", "Basic "+authb64.decode())
        else:
                request = urllib.request.Request(url)
        response = urllib.request.urlopen(request)
        contents = response.read().decode("utf-8")
        try:
                data = json.loads(contents)
        except:
                # If it is not json, try to add the optional subdirectory tag to the url.
                # Then return a repo file, if one is found in the listing (good for RPMs)
                # or return the url to the directory (good for DEBs).
                request.selector += '/' + tag
                try:
                        response = urllib.request.urlopen(request)
                        contents = response.read().decode("utf-8")
                        url += '/' + tag
                except:
                        pass
                repofiles = re.findall('">([^<\s]+\.repo)<', contents, re.MULTILINE)
                if len(repofiles): return url+'/'+repofiles[0]
                return url

        # FIXME: find how this is configured/if there's a way to get it included on the json/peek at the directory to match *.repo
        subfix = parse_qs(urlparse(url).query)["project"][0] + '.repo'

        print("tag=", tag, "; data=", data)
        if not 'repo' in data[tag]:
                print("ERROR: no repo in data['"+tag+"']=", data[tag])
        # workaround for https://github.com/openSUSE/software-o-o/issues/123
        data[tag]['repo'] = data[tag]['repo'].replace('https://', 'http://', 1)

        if ('Ubuntu' in distribution or 'Linux_Mint' in distribution or distribution == 'Debian'):
                return data[tag]['repo']
        else:
                return data[tag]['repo']+subfix

if __name__ == '__main__':

        parser = OptionParser()

        parser.add_option("-u", "--url",
                            help="Specify the repository json description.")
        parser.add_option("-d", "--distribution",
                            help="Specify the Linux distribution.")
        parser.add_option("-v", "--version",
                            help="Specify the distribution version.")
        parser.add_option("-r", "-o", "--override",
                            help="Override the repository descriptor with an URL. Use 'https://...*.repo' syntax for RPM, and 'http://...' for DEB")
        parser.add_option("-k", "--key",
                            help="Override the repository key default URL.")

        (options, args) = parser.parse_args()

        url             = options.url
        distribution    = options.distribution
        version         = options.version
        target_url      = options.override
        key_url         = options.key

        if target_url is None:
                target_url = get_repo_url(url,distribution,version)
        os.environ["REPOSITORY"] = target_url

        # URL valid/used only for Debian/Ubuntu repositories
        is_rpmish = re.subn("/[^/]+\.repo$", "", target_url)
        if is_rpmish[1]:
                os.environ["REPOSITORY_KEY"] = is_rpmish[0]+'/repodata/repomd.xml.key'
        else:
                os.environ["REPOSITORY_KEY"] = target_url+'/Release.key'
 
        if key_url is not None:
                os.environ["REPOSITORY_KEY"] = key_url

        # Workaround for SUSE.sh
        distribution = distribution if not "SUSE" in distribution else "SUSE"
        # ... the same for the xUbuntu notation
        distribution = distribution if not "Ubuntu" in distribution else "Ubuntu"

        for e in ('REPOSITORY', 'REPOSITORY_KEY'):
          print('+ env %s=%s' % (e, os.environ[e]))
        print('+ before_install/'+distribution+'.sh')
        subprocess.call('before_install/'+distribution+'.sh')
