#!/usr/bin/env bash
set -xe

# rpm --import $REPOSITORY_KEY  # not needed when using gpg-auto-import feature
zypper addrepo $REPOSITORY
zypper --gpg-auto-import-keys refresh
