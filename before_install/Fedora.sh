#!/usr/bin/env bash
set -xe

dnf install -y 'dnf-command(config-manager)'
dnf config-manager --add-repo $REPOSITORY
