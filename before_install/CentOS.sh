#!/usr/bin/env bash
set -xe

yum install -y wget
cd /etc/yum.repos.d/
wget $REPOSITORY

rpm --import $REPOSITORY_KEY
yum clean expire-cache

# Only needed for RHEL installations:
if [ "$0" == "before_install/RHEL.sh" ]; then
    wget https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
    yum install -y epel-release-latest-7.noarch.rpm
    yum install -y python34
fi
