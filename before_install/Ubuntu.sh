#!/usr/bin/env bash
set -xe

wget $REPOSITORY_KEY -O /tmp/Release.key
apt-key add - < /tmp/Release.key
rm -f /tmp/Release.key
echo "================================================================="
echo "WARNING: look for GPG errors! We take them as warnings only here."
echo "================================================================="
echo "deb [allow-insecure=yes] $REPOSITORY /" > /etc/apt/sources.list.d/$LINUX_PACKAGE_SHORTNAME-client.list
apt-get --force-yes --allow-unauthenticated update
